import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCount {
    public static void main(String[] args) throws Exception {

        // Create a job by providing the configuration and a text description of the task
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "word count");

        // We specify the classes WordCount, Map, Combine and Reduce
        job.setJarByClass(WordCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);

        // Definition of the key / value types of our problem
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // Definition of input and output files (here considered as arguments to be specified during execution)
        // Using single Input file
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        // run the job
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}